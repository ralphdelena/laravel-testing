@extends('layouts.auth')

@section('content')
<div>
    <div class="page-header">
      <h3 class="page-title">Dashboard</h3>
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="{{ url('/home') }}">Dashboard</a>
          </li>
          <li class="breadcrumb-item" aria-current="page">
            <a href="{{ route('roles.index') }}">Role</a>
          </li>
        </ol>
      </nav>
    </div>

    <div class="card">
      <div class="card-header">
        <i class="fa fa-align-justify"></i> Roles Listing
        <a class="float-right" href="{{ route('roles.create') }}"> + Create New Role</a>
      </div>
      <div class="card-body">
      <table class="table table-responsive-sm mt-3">
        <tr>
          <th>No</th>
          <th>Name</th>
          <th width="280px">Action</th>
        </tr>
          @foreach ($roles as $key => $role)
        <tr>
          <td>{{ ++$i }}</td>
          <td>{{ $role->name }}</td>
          <td>
          <a class="btn btn-info" href="{{ route('roles.show',$role->id) }}">Show</a>
          @can('role-edit')
          <a class="btn btn-primary" href="{{ route('roles.edit',$role->id) }}">Edit</a>
          @endcan
          @can('role-delete')
          {!! Form::open(['method' => 'DELETE','route' => ['roles.destroy', $role->id],'style'=>'display:inline']) !!}
          {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
          {!! Form::close() !!}
          @endcan
          </td>
        </tr>
        @endforeach
        </table>
      </div>
    </div>
  </div>

@endsection
