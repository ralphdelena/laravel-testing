@extends('layouts.auth')
@section('content')



<div>
    <div class="page-header">
      <h3 class="page-title">Dashboard</h3>
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href>Dashboard</a>
          </li>
          <li class="breadcrumb-item" aria-current="page">
            <a class="float-right" href="{{ route('roles.index') }}">Roles</a>
          </li>
          <li class="breadcrumb-item active" aria-current="page">Edit New Role</li>
        </ol>
      </nav>
    </div>

    <div class="card">
      <div class="card-header">
      Edit New User
        <div class="card-header-actions">
          <a class="float-right" href="{{ route('roles.index') }}">Back</a>
        </div>
        <!--card-header-actions-->
      </div>
      <!--card-header-->

      <div class="card-body">
      {!! Form::model($role, ['method' => 'PATCH','route' => ['roles.update', $role->id]]) !!}
        <div class="form-group row">
            <label for="name" class="col-md-2 col-form-label">Username</label>
            <div class="col-md-10">
            {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
            </div>
         </div>
          
         @foreach($permission as $value)
          <label>{{ Form::checkbox('permission[]', $value->id, in_array($value->id, $rolePermissions) ? true : false, array('class' => 'name')) }}
          {{ $value->name }}</label>
          <br/>
          @endforeach
          

          <button class="btn btn-sm btn-primary float-right" type="submit">Update Role</button>
          {!! Form::close() !!}
      </div>
    </div>
  </div>

  @endsection