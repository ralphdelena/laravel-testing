<div class="c-sidebar c-sidebar-dark c-sidebar-fixed c-sidebar-lg-show" id="sidebar">
      <div class="c-sidebar-brand d-lg-down-none">
        <svg class="c-sidebar-brand-full" width="118" height="46" alt="CoreUI Logo">
          <use xlink:href="assets/brand/coreui.svg#full"></use>
        </svg>
        <svg class="c-sidebar-brand-minimized" width="46" height="46" alt="CoreUI Logo">
          <use xlink:href="assets/brand/coreui.svg#signet"></use>
        </svg>
      </div>
      <ul class="c-sidebar-nav">
        <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="{{ url('/home') }}">
            <svg class="c-sidebar-nav-icon">
              <use xlink:href="node_modules/@coreui/icons/sprites/free.svg#cil-speedometer"></use>
            </svg> Dashboard</a></li>
        <li class="c-sidebar-nav-title">User Management</li>
        <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="{{ url('/admin/departments') }}">
            <svg class="c-sidebar-nav-icon">
              <use xlink:href="node_modules/@coreui/icons/sprites/free.svg#cil-drop1"></use>
            </svg> Departments</a></li>
        <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="{{ url('/admin/employees') }}">
            <svg class="c-sidebar-nav-icon">
              <use xlink:href="node_modules/@coreui/icons/sprites/free.svg#cil-pencil"></use>
            </svg> Employees</a></li>
        <li class="c-sidebar-nav-title">Settings</li>
        <li class="c-sidebar-nav-item c-sidebar-nav-dropdown"><a class="c-sidebar-nav-link c-sidebar-nav-dropdown-toggle" href="#">
            <svg class="c-sidebar-nav-icon">
              <use xlink:href="node_modules/@coreui/icons/sprites/free.svg#cil-puzzle"></use>
            </svg> Settings</a>
          <ul class="c-sidebar-nav-dropdown-items">
            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="{{ url('/admin/cities') }}"><span class="c-sidebar-nav-icon"></span> Cities</a></li>
            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="{{ url('/admin/states') }}"><span class="c-sidebar-nav-icon"></span> States</a></li>
            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="{{ url('/admin/countries') }}"><span class="c-sidebar-nav-icon"></span> Countries</a></li>
            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="{{ url('/admin/roles') }}"><span class="c-sidebar-nav-icon"></span> Roles</a></li>
            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="{{ url('/admin/users') }}"><span class="c-sidebar-nav-icon"></span> Users</a></li>
          </ul>
        </li>
 
        
       
      </ul>
      </div>