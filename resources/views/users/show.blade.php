@extends('layouts.auth')
@section('content')



<div>
    <div class="page-header">
      <h3 class="page-title">Dashboard</h3>
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href>Dashboard</a>
          </li>
          <li class="breadcrumb-item" aria-current="page">
            <a class="float-right" href="{{ route('users.index') }}">User</a>
          </li>
          <li class="breadcrumb-item active" aria-current="page">Edit New User</li>
        </ol>
      </nav>
    </div>

    <div class="card">
      <div class="card-header">
      User Info
        <div class="card-header-actions">
          <a class="float-right" href="{{ route('users.index') }}">Back</a>
        </div>
        <!--card-header-actions-->
      </div>
      <!--card-header-->

      <div class="card-body">

        <div class="form-group row">
            <label for="name" class="col-md-2 col-form-label">Username</label>
            <div class="col-md-10">
            {!! Form::text('username', null, array('placeholder' => 'Username','class' => 'form-control')) !!}
            </div>
         </div>
          <div class="form-group row">
            <label for="name" class="col-md-2 col-form-label">Last Name</label>
            <div class="col-md-10">
            {!! Form::text('last_name', null, array('placeholder' => 'Last Name','class' => 'form-control')) !!}
            </div>
            </div>
          <div class="form-group row">
            <label for="name" class="col-md-2 col-form-label">First Name</label>
            <div class="col-md-10">
            {!! Form::text('first_name', null, array('placeholder' => 'First Name','class' => 'form-control')) !!}
            </div>
            </div>
          <div class="form-group row">
            <label for="name" class="col-md-2 col-form-label">Email</label>
            <div class="col-md-10">
            {!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control')) !!}
            </div>
        </div>
         
        <div class="form-group row">
            <label for="name" class="col-md-2 col-form-label">Roles</label>
            <div class="col-md-10">
            @if(!empty($user->getRoleNames()))
        @foreach($user->getRoleNames() as $v)
        <label class="badge badge-success">{{ $v }}</label>
        @endforeach
        @endif
            </div>
        </div>
        
      </div>
    </div>
  </div>

  @endsection