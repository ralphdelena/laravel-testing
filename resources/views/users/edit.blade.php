@extends('layouts.auth')
@section('content')



<div>
    <div class="page-header">
      <h3 class="page-title">Dashboard</h3>
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href>Dashboard</a>
          </li>
          <li class="breadcrumb-item" aria-current="page">
            <a class="float-right" href="{{ route('users.index') }}">User</a>
          </li>
          <li class="breadcrumb-item active" aria-current="page">Edit New User</li>
        </ol>
      </nav>
    </div>

    <div class="card">
      <div class="card-header">
      Edit New User
        <div class="card-header-actions">
          <a class="float-right" href="{{ route('users.index') }}">Back</a>
        </div>
        <!--card-header-actions-->
      </div>
      <!--card-header-->

      <div class="card-body">
      {!! Form::model($user, ['method' => 'PATCH','route' => ['users.update', $user->id]]) !!}
        <div class="form-group row">
            <label for="name" class="col-md-2 col-form-label">Username</label>
            <div class="col-md-10">
            {!! Form::text('username', null, array('placeholder' => 'Username','class' => 'form-control')) !!}
            </div>
         </div>
          <div class="form-group row">
            <label for="name" class="col-md-2 col-form-label">Last Name</label>
            <div class="col-md-10">
            {!! Form::text('last_name', null, array('placeholder' => 'Last Name','class' => 'form-control')) !!}
            </div>
            </div>
          <div class="form-group row">
            <label for="name" class="col-md-2 col-form-label">First Name</label>
            <div class="col-md-10">
            {!! Form::text('first_name', null, array('placeholder' => 'First Name','class' => 'form-control')) !!}
            </div>
            </div>
          <div class="form-group row">
            <label for="name" class="col-md-2 col-form-label">Email</label>
            <div class="col-md-10">
            {!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control')) !!}
            </div>
        </div>
          <div class="form-group row">
            <label for="name" class="col-md-2 col-form-label">Password</label>
            <div class="col-md-10">
            {!! Form::password('password', array('placeholder' => 'Password','class' => 'form-control')) !!}
            </div>
        </div>
          <div class="form-group row">
            <label for="name" class="col-md-2 col-form-label">Confirm Password</label>
            <div class="col-md-10">
            {!! Form::password('confirm-password', array('placeholder' => 'Confirm Password','class' => 'form-control')) !!}
            </div>
        </div>
          <div class="form-group row">
            <label for="name" class="col-md-2 col-form-label">Role</label>
            <div class="col-md-10">
            {!! Form::select('roles[]', $roles,$userRole, array('class' => 'form-control','multiple')) !!}
            </div>
          </div>

          <button class="btn btn-sm btn-primary float-right" type="submit">Update User</button>
          {!! Form::close() !!}
      </div>
    </div>
  </div>

  @endsection