/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("./bootstrap");

window.Vue = require("vue");
import VueRouter from "vue-router";
import VueGoodTable from "vue-good-table";

Vue.use(VueGoodTable);
window.Vue.component("pagination", require("laravel-vue-pagination"));

window.Vue.use(VueRouter);

import UsersIndex from "./components/users/UsersIndex.vue";
import UsersCreate from "./components/users/UsersCreate.vue";
import UsersEdit from "./components/users/UsersEdit.vue";

import RolesIndex from "./components/roles/RolesIndex.vue";
import RolesCreate from "./components/roles/RolesCreate.vue";
import RolesEdit from "./components/roles/RolesEdit.vue";

import PermissionsIndex from "./components/permissions/PermissionsIndex.vue";
import PermissionsCreate from "./components/permissions/PermissionsCreate.vue";
import PermissionsEdit from "./components/permissions/PermissionsEdit.vue";

import CountriesIndex from "./components/countries/CountriesIndex.vue";
import CountriesCreate from "./components/countries/CountriesCreate.vue";
import CountriesEdit from "./components/countries/CountriesEdit.vue";

import StatesIndex from "./components/states/StatesIndex.vue";
import StatesCreate from "./components/states/StatesCreate.vue";
import StatesEdit from "./components/states/StatesEdit.vue";

import CitiesIndex from "./components/cities/CitiesIndex.vue";
import CitiesCreate from "./components/cities/CitiesCreate.vue";
import CitiesEdit from "./components/cities/CitiesEdit.vue";

import DepartmentsIndex from "./components/departments/DepartmentsIndex.vue";
import DepartmentsCreate from "./components/departments/DepartmentsCreate.vue";
import DepartmentsEdit from "./components/departments/DepartmentsEdit.vue";

import EmployeesIndex from "./components/employees/EmployeesIndex.vue";
import EmployeesCreate from "./components/employees/EmployeesCreate.vue";
import EmployeesEdit from "./components/employees/EmployeesEdit.vue";

const routes = [
    {
        path: "/",
        components: {
            usersIndex: UsersIndex,
            rolesIndex: RolesIndex,
            countriesIndex: CountriesIndex,
            statesIndex: StatesIndex,
            citiesIndex: CitiesIndex,
            departmentsIndex: DepartmentsIndex,
            employeesIndex: EmployeesIndex,
            permissionsIndex: PermissionsIndex
        }
    },
    {
        path: "/users/create",
        component: UsersCreate,
        name: "createUser"
    },
    {
        path: "/users/edit/:id",
        component: UsersEdit,
        name: "editUser"
    },
    {
        path: "/roles/create",
        component: RolesCreate,
        name: "createRole"
    },
    {
        path: "/roles/edit/:id",
        component: RolesEdit,
        name: "editRole"
    },
    {
        path: "/permissions/create",
        component: PermissionsCreate,
        name: "createPermission"
    },
    {
        path: "/permissions/edit/:id",
        component: PermissionsEdit,
        name: "editPermission"
    },
    {
        path: "/countries/create",
        component: CountriesCreate,
        name: "createCountry"
    },
    {
        path: "/countries/edit/:id",
        component: CountriesEdit,
        name: "editCountry"
    },
    {
        path: "/states/create",
        component: StatesCreate,
        name: "createState"
    },
    {
        path: "/states/edit/:id",
        component: StatesEdit,
        name: "editState"
    },
    {
        path: "/cities/create",
        component: CitiesCreate,
        name: "createCity"
    },
    {
        path: "/cities/edit/:id",
        component: CitiesEdit,
        name: "editCity"
    },
    {
        path: "/departments/create",
        component: DepartmentsCreate,
        name: "createDepartment"
    },
    {
        path: "/departments/edit/:id",
        component: DepartmentsEdit,
        name: "editDepartment"
    },
    {
        path: "/employees/create",
        component: EmployeesCreate,
        name: "createEmployee"
    },
    {
        path: "/employees/edit/:id",
        component: EmployeesEdit,
        name: "editEmployee"
    }
];

const router = new VueRouter({ routes });

const app = new Vue({ router }).$mount("#app");
