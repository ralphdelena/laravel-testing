<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => '/v1'], function () {
    // Route::resource('users', 'Api\v1\UsersController');
    // Route::resource('users', 'App\Http\Controllers\Api\v1\UsersController');
    // Route::resource('roles', 'App\Http\Controllers\Api\v1\RolesController');
    // Route::resource('permissions', 'App\Http\Controllers\Api\v1\PermissionsController');
    Route::resource('countries', 'App\Http\Controllers\Api\v1\CountriesController');
    Route::get('countries/states/{id}', 'App\Http\Controllers\Api\v1\CountriesController@show_states');
    Route::get('states/cities/{id}', 'App\Http\Controllers\Api\v1\StatesController@show_cities');
    Route::resource('states', 'App\Http\Controllers\Api\v1\StatesController');
    Route::resource('cities', 'App\Http\Controllers\Api\v1\CitiesController');
    Route::resource('employees', 'App\Http\Controllers\Api\v1\EmployeesController');
    Route::resource('departments', 'App\Http\Controllers\Api\v1\DepartmentsController');
   
});


