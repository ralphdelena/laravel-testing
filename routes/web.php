<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('home', 'App\Http\Controllers\HomeController@index')->name('home');
Route::group(['prefix' => 'admin'], function () {
    Route::resource('users', 'App\Http\Controllers\Admin\UsersController');
    Route::resource('roles', 'App\Http\Controllers\Admin\RolesController');
    Route::get('permissions', 'App\Http\Controllers\Admin\PageController@permission_index');
    Route::get('countries', 'App\Http\Controllers\Admin\PageController@country_index');
    Route::get('states', 'App\Http\Controllers\Admin\PageController@state_index');
    Route::get('cities', 'App\Http\Controllers\Admin\PageController@city_index');
    Route::get('departments', 'App\Http\Controllers\Admin\PageController@department_index');
    Route::get('employees', 'App\Http\Controllers\Admin\PageController@employee_index');
    

});


