<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employee extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = [
        'last_name', 'first_name','address', 'department_id', 'city_id', 'state_id','country_id', 'zip',
        'birthdate', 'date_hired'
    ];
    public function countries(){
        return $this->belongsTo('App\Models\Country','name');
    }
    public function departments(){
        return $this->belongsTo('App\Models\Department','name');
    }
    public function states(){
        return $this->belongsTo('App\Models\State','name');
    }
    public function city(){
        return $this->belongsTo('App\Models\City','name');
    }
}

