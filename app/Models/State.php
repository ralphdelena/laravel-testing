<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    use HasFactory;
    protected $fillable = [
        'country_id', 'name',
    ];
    public function countries(){
        return $this->belongsTo('App\Models\Country','name');
    }
    public function cities(){
        return $this->hasMany('App\Models\City');
    }
    public function employees(){
        return $this->hasMany('App\Models\Employee');
    }
}
