<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    use HasFactory;
    protected $fillable = [
        'state_id', 'name',
    ];
    public function states(){
        return $this->belongsTo('App\Models\State','name');
    }
    public function employees(){
        return $this->hasMany('App\Models\Employee');
    }
}
