<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Permission;
use App\Models\Role;
use App\Models\User;

class PermissionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function Permission()
    {   
    	$dev_permission = Permission::where('display_name','create-tasks')->first();
		$manager_permission = Permission::where('display_name', 'edit-users')->first();

		//RoleTableSeeder.php
		$dev_role = new Role();
		$dev_role->display_name = 'developer';
        $dev_role->name = 'Front-end Developer';
        $dev_role->description = 'Front-end Developer';
		$dev_role->save();
		$dev_role->permissions()->attach($dev_permission);

		$manager_role = new Role();
		$manager_role->display_name = 'manager';
        $manager_role->name = 'Assistant Manager';
        $manager_role->description = 'Assistant Manager';
		$manager_role->save();
		$manager_role->permissions()->attach($manager_permission);

		$dev_role = Role::where('display_name','developer')->first();
		$manager_role = Role::where('display_name', 'manager')->first();

		$createTasks = new Permission();
		$createTasks->display_name = 'create-tasks';
        $createTasks->name = 'Create Tasks';
        $createTasks->description = 'Create Tasks';
		$createTasks->save();
		$createTasks->roles()->attach($dev_role);

		$editUsers = new Permission();
		$editUsers->display_name = 'edit-users';
        $editUsers->name = 'Edit Users';
        $editUsers->description = 'Edit Users';
		$editUsers->save();
		$editUsers->roles()->attach($manager_role);

		$dev_role = Role::where('display_name','developer')->first();
		$manager_role = Role::where('display_name', 'manager')->first();
		$dev_perm = Permission::where('display_name','create-tasks')->first();
		$manager_perm = Permission::where('display_name','edit-users')->first();

		$developer = new User();
        $developer->username = 'dev';
        $developer->last_name = 'mister';
        $developer->first_name = 'dev';
		$developer->email = 'dev@dev.dev';
		$developer->password = bcrypt('secrettt');
		$developer->save();
		$developer->roles()->attach($dev_role);
		$developer->permissions()->attach($dev_perm);

        $manager = new User();
        $manager->username = 'manager';
        $manager->last_name = 'mister';
        $manager->first_name = 'manager';
		$manager->email = 'manager@manager.manager';
		$manager->password = bcrypt('secrettt');
		$manager->save();
		$manager->roles()->attach($manager_role);
		$manager->permissions()->attach($manager_perm);

		
		return redirect()->back();
    }
}
