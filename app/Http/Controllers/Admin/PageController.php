<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Role;
use App\Models\Permission;
use App\Models\Country;
use App\Models\State;
use App\Models\City;
use App\Models\Department;
use App\Models\Employee;

class PageController extends Controller
{
    public function user_index()
    {
         return view('users.index');
    }
    public function role_index()
    {
         return view('roles.index');
    }
    public function permission_index()
    {
         return view('permissions.index');
    }
    public function country_index()
    {
         return view('countries.index');
    }
    public function state_index()
    {
         return view('states.index');
    }
    public function city_index()
    {
         return view('cities.index');
    }
    public function department_index()
    {
         return view('departments.index');
    }
    public function employee_index()
    {
         return view('employees.index');
    }
}
