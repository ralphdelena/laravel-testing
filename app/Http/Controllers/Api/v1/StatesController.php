<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Models\Country;
use App\Models\State;
use App\Models\City;
use DB;
class StatesController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $states = DB::table('states')
        ->select('states.id','states.name','states.country_id','countries.name as country')
        ->join('countries','countries.id','=','states.country_id')
        ->get();
        // $states = State::with('countries')->get();
        return response()->json($states);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        State::create([
                'name' => $data['name'],
                'country_id' => $data['country_id'],
        ])->save();

        return $this->respondAccepted();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $state = DB::table('states')
        ->select('states.id','states.name','states.country_id','countries.name as country')
        ->where('states.id', $id)
        ->join('countries','countries.id','=','states.country_id')
        ->first();
        // $states = State::with('countries')->get();
        return response()->json($state);
    }
    public function show_cities($id)
    {
        return City::where('state_id',$id)->get();
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $collection = State::find($id); 
        if(! $collection){
            return $this->respondNotFound();
        }
        $input = $request->all();
        $collection->fill($input)->save();
        return $this->respondSuccess();
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $collection = State::find($id);
        if(! $collection){
            return $this->respondNotFound();
        } 
        $collection->delete();
        return $this->respondSuccess();
    }

    public function transform($collection)
    {
        $fk_id = $collection['country_id'];
        $fk = Country::find($fk_id);
        return [
            'id' => $collection['id'],
            'name' => $collection['name'],
            'country' => $fk['name']
        ];

    }
    private function transformWithPaginate($collections)
    {
        
        $itemsTransformed = $collections
            ->getCollection()
            ->map(function($collection) {
                $fk_id = $collection['country_id'];
                $fk = Country::find($fk_id);
                return [
                    'id' => $collection['id'],
                    'name' => $collection['name'],
                    'country' => $fk['name']
                ];
        });

        return $itemsTransformed;
    }
}
