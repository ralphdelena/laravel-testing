<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Models\Department;
use App\Models\Employee;
use App\Models\City;
use App\Models\State;
use App\Models\Country;
use DB;

class EmployeesController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $null = null;
        $cities = DB::table('employees')
        
        ->select('employees.id',
                'employees.last_name',
                'employees.first_name',
                'employees.address',
                'employees.department_id',
                'employees.city_id',
                'employees.state_id',
                'employees.country_id',
                'employees.zip',
                'employees.birthdate',
                'employees.date_hired',
                'departments.name as department',
                'cities.name as city',
                'states.name as state',
                'countries.name as country')
        ->join('departments','departments.id','=','employees.department_id')
        ->join('cities','cities.id','=','employees.city_id')
        ->join('states','states.id','=','employees.state_id')
        ->join('countries','countries.id','=','employees.country_id')
        ->get();
        // $states = State::with('countries')->get();
        return response()->json($cities);
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        Employee::create([
                'last_name' => $data['last_name'],
                'first_name' => $data['first_name'],
                'address' => $data['address'],
                'department_id' => $data['department_id'],
                'city_id' => $data['city_id'],
                'state_id' => $data['state_id'],
                'country_id' => $data['country_id'],
                'zip' => $data['zip'],
                'birthdate' => $data['birthdate'],
                'date_hired' => $data['date_hired']

        ])->save();

        return $this->respondAccepted();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cities = DB::table('employees')
        ->select('employees.id',
                'employees.last_name',
                'employees.first_name',
                'employees.address',
                'employees.department_id',
                'employees.city_id',
                'employees.state_id',
                'employees.country_id',
                'employees.zip',
                'employees.birthdate',
                'employees.date_hired',
                'departments.name as department',
                'cities.name as city',
                'states.name as state',
                'countries.name as country')
        ->where('employees.id', $id)
        ->join('departments','departments.id','=','employees.department_id')
        ->join('cities','cities.id','=','employees.city_id')
        ->join('states','states.id','=','employees.state_id')
        ->join('countries','countries.id','=','employees.country_id')
        ->first();
        // $states = State::with('countries')->get();
        return response()->json($cities);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $collection = Employee::find($id); 
        if(! $collection){
            return $this->respondNotFound();
        }
        $input = $request->all();
        $collection->fill($input)->save();
        return $this->respondSuccess();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $collection = Employee::find($id);
        if(! $collection){
            return $this->respondNotFound();
        } 
        $collection->delete();
        return $this->respondSuccess();
    }

    public function transform($collection)
    {
        $fk_id1 = $collection['department_id'];
        $f1k = Department::find($fk_id1);

        $fk_id2 = $collection['department_id'];
        $fk2 = City::find($fk_id2);

        $fk_id3 = $collection['department_id'];
        $fk3 = State::find($fk_id3);

        $fk_id4 = $collection['department_id'];
        $fk4 = Country::find($fk_id4);

        return [
            'id' => $collection['id'],
            'last_name' => $collection['last_name'],
            'first_name' => $collection['first_name'],
            'middle_name' => $collection['middle_name'],
            'address' => $collection['address'],
            'department' => $fk1['department_id'],
            'city' => $fk2['city_id'],
            'state' => $fk3['state_id'],
            'city' => $fk4['city_id'],
            'zip' => $collection['zip'],
            'birthdate' => $collection['birthdate'],
            'date_hired' => $collection['date_hired']
        ];

    }
    private function transformWithPaginate($collections)
    {
        
        $itemsTransformed = $collections
            ->getCollection()
            ->map(function($collection) {
                $fk_id1 = $collection['department_id'];
                $f1k = Department::find($fk_id1);

                $fk_id2 = $collection['department_id'];
                $fk2 = City::find($fk_id2);

                $fk_id3 = $collection['department_id'];
                $fk3 = State::find($fk_id3);

                $fk_id4 = $collection['department_id'];
                $fk4 = Country::find($fk_id4);

                return [
                    'id' => $collection['id'],
                    'last_name' => $collection['last_name'],
                    'first_name' => $collection['first_name'],
                    'middle_name' => $collection['middle_name'],
                    'address' => $collection['address'],
                    'department' => $fk1['department_id'],
                    'city' => $fk2['city_id'],
                    'state' => $fk3['state_id'],
                    'city' => $fk4['city_id'],
                    'zip' => $collection['zip'],
                    'birthdate' => $collection['birthdate'],
                    'date_hired' => $collection['date_hired']
                ];
        })->toArray();

        return $itemsTransformed;
    }
}
