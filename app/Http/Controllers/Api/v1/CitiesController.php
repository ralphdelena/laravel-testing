<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Models\City;
use App\Models\State;
use DB;

class CitiesController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cities = DB::table('cities')
        ->select('cities.id','cities.name','cities.state_id','states.name as state')
        ->join('states','states.id','=','cities.state_id')
        ->get();
        // $states = State::with('countries')->get();
        return response()->json($cities);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        City::create([
                'name' => $data['name'],
                'state_id' => $data['state_id'],
        ])->save();

        return $this->respondAccepted();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cities = DB::table('cities')
        ->select('cities.id','cities.name','cities.state_id','states.name as state')
        ->where('cities.id',$id)
        ->join('states','states.id','=','cities.state_id')
        ->first();
        // $states = State::with('countries')->get();
        return response()->json($cities);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $collection = City::find($id); 
        if(! $collection){
            return $this->respondNotFound();
        }
        $input = $request->all();
        $collection->fill($input)->save();
        return $this->respondSuccess();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $collection = City::find($id);
        if(! $collection){
            return $this->respondNotFound();
        } 
        $collection->delete();
        return $this->respondSuccess();
    }

    public function transform($collection)
    {
        $fk_id = $collection['state_id'];
        $fk = State::find($fk_id);
        return [
            'id' => $collection['id'],
            'name' => $collection['name'],
            'state' => $fk['name']
        ];

    }
    private function transformWithPaginate($collections)
    {
        
        $itemsTransformed = $collections
            ->getCollection()
            ->map(function($collection) {
                $fk_id = $collection['state_id'];
                $fk = State::find($fk_id);
                return [
                    'id' => $collection['id'],
                    'name' => $collection['name'],
                    'state' => $fk['name']
                ];
        })->toArray();

        return $itemsTransformed;
    }
}
