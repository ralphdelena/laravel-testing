<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

use App\Http\Resources\Users as UserResource;
use App\Models\Role;
use App\Models\Permission;
use App\Models\User;
use DB;


class UsersController extends ApiController
{

    public function index(Request $request)
    {
        $data = User::orderBy('id','DESC')->paginate(5);
        return view('users.index',compact('data'))
        ->with('i', ($request->input('page', 1) - 1) * 5);
    }
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $roles = Role::pluck('name','name')->all();
        return view('users.create',compact('roles'));
    }

 
    public function store(Request $request)
    {
        $data = $request->all();
        User::create([
            'id' => $data['id'],
            'username' => $data['username'],
            'last_name' => $data['last_name'],
            'first_name' => $data['first_name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ])->save();

        return $this->respondAccepted();
    }

 
    public function show($id)
    {
        return User::findOrFail($id);
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        $user = User::find($id); 
        if(! $user){
            return $this->respondNotFound();
        }
        $data = $request->all();
        $user->fill([
            'id' => $data['id'],
            'username' => $data['username'],
            'last_name' => $data['last_name'],
            'first_name' => $data['first_name'],
            'email' => $data['email']
        ])->save();

        return $this->respondSuccess();
    }

  
    public function destroy($id)
    {
        $user = User::find($id);
        if(! $user){
            return $this->respondNotFound();
        } 
        $user->delete();
        return $this->respondSuccess();
    }
    public function transform($user)
    {
        return [
            'id' => $user['id'],
            'username' => $user['username'],
            'first_name' => $user['first_name'],
            'last_name' => $user['last_name'],
            'email' => $user['email'],
            'created_at' => $user['created_at'],
            'updated_at' => $user['updated_at']
        ];

    }
    private function transformWithPaginate($users)
    {
        
        $itemsTransformed = $users
            ->getCollection()
            ->map(function($user) {
                return [
                    'id' => $user['id'],
                    'username' => $user['username'],
                    'first_name' => $user['first_name'],
                    'last_name' => $user['last_name'],
                    'email' => $user['email'],
                    'created_at' => $user['created_at'],
                    'updated_at' => $user['updated_at']
                ];
        })->toArray();

        return $itemsTransformed;
    }
}
