<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Models\Country;
use App\Models\State;
use App\Models\City;
class CountriesController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(Country::get());
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        Country::create([
                'code' => $data['code'],
                'name' => $data['name']
        ])->save();

        return $this->respondAccepted();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  
    public function show($id)
    {
        return Country::findOrFail($id);
    }
    public function show_states($id)
    {
        return State::where('country_id',$id)->get();
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $collection = Country::find($id); 
        if(! $collection){
            return $this->respondNotFound();
        }
        $input = $request->all();
        $collection->fill($input)->save();
        return $this->respondSuccess();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $collection = Country::find($id);
        if(! $collection){
            return $this->respondNotFound();
        } 
        $collection->delete();
        return $this->respondSuccess();
    }
    public function transform($collection)
    {
        return [
            'name' => $collection['name']
        ];

    }
    private function transformWithPaginate($collections)
    {
        
        $itemsTransformed = $collections
            ->getCollection()
            ->map(function($collection) {
                return [
                    'name' => $collection['name']
                ];
        })->toArray();

        return $itemsTransformed;
    }
}
